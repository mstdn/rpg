# Roleplaying Game UE 5.1.x

Welcome to the RPG repository, on here you will find all project files and assets needed to compile the RPG game. This game is still very much in development and not ready for production. Additions and updates will be added to this repo as we go!


## Features roadmap

- [x] Crouching, running and sprinting
- [x] Vaulting
- [x] Melee sword combo
- [x] Target lock, dodge roll
- [x] Player Stats
- [ ] Armor Equipables
- [ ] Adding items
- [ ] Open World Map
- [ ] AI System
- [ ] Zones
- [ ] Player Minimap
- [ ] Save & Load System
- [ ] Front-end Widgets

## Bug'z

- [ ] IsAttacking check on assassination
- [ ] IsAttacking check on roll 


#### Quick start

Download the latest source code,unzip and open the 'RolePlay.uproject' to launch the project in Unreal Engine.
 